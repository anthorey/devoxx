import Vue from "vue";
import { EventsVue } from "./primary/events";
import { CalendarEvents } from "@/domain/CalendarEvents";
import { CalendarEventsGoogle } from "@/secondary/CalendarEventsGoogle";
import axios from "axios";

Vue.config.productionTip = false;

const calendarEvents: CalendarEvents = new CalendarEventsGoogle(axios.create());

new Vue({
  render: (h) => h(EventsVue),
  provide: {
    calendarEvents: () => calendarEvents,
  },
}).$mount("#app");
