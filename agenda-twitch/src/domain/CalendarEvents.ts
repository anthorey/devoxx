import { CalendarEvent } from "@/domain/CalendarEvent";

export interface CalendarEvents {
  listNext(now: Date): Promise<CalendarEvent[]>;
}
