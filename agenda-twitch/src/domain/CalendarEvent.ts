type Title = string;
type Id = string;

export interface CalendarEvent {
  id: Id;
  title: Title;
  start: Date;
  end: Date;
}
