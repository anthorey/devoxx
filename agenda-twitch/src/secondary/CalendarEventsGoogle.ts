import { CalendarEvents } from "@/domain/CalendarEvents";
import { AxiosInstance, AxiosResponse } from "axios";
import { CalendarEvent } from "@/domain/CalendarEvent";
import { toCalendarEvents } from "@/secondary/ics";
import * as ical from "ical";

const byStartTime = (
  { start: first }: CalendarEvent,
  { start: second }: CalendarEvent
): number => first.getTime() - second.getTime();

const after =
  (now: Date) =>
  ({ end }: CalendarEvent) =>
    end.getTime() > now.getTime();

const fromResponseToCalendarEvents = (now: Date) => {
  return (response: AxiosResponse<string>): CalendarEvent[] =>
    toCalendarEvents(ical.parseICS(response.data))
      .filter(after(now))
      .sort(byStartTime);
};

export class CalendarEventsGoogle implements CalendarEvents {
  constructor(private readonly instance: AxiosInstance) {}

  listNext(now: Date): Promise<CalendarEvent[]> {
    return this.instance
      .get("calendar.ics")
      .then(fromResponseToCalendarEvents(now));
  }
}
