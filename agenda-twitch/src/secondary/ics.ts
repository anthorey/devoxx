import { CalendarEvent } from "@/domain/CalendarEvent";
import { CalendarComponent, FullCalendar } from "ical";

export type CalendarElement = [string, CalendarComponent];

const checkPresence = <T>(field: string, option: T | undefined): T => {
  if (option === undefined) {
    throw new Error(`The field ${field} should be present.`);
  }
  return option;
};

export const toCalendarEvent = ([
  id,
  { summary: title, start, end },
]: CalendarElement): CalendarEvent => ({
  id,
  title: checkPresence("title", title),
  start: checkPresence("start", start),
  end: checkPresence("end", end),
});

export const toCalendarEvents = (fullCalendar: FullCalendar): CalendarEvent[] =>
  Object.entries(fullCalendar).map(toCalendarEvent);
