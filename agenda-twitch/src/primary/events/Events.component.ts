import { Component, Inject, Vue } from "vue-property-decorator";
import { CalendarEvent } from "@/domain/CalendarEvent";
import { CalendarEvents } from "@/domain/CalendarEvents";

@Component
export default class EventsComponent extends Vue {
  events: CalendarEvent[] = [];

  @Inject()
  private calendarEvents!: () => CalendarEvents;

  created(): void {
    this.calendarEvents()
      .listNext(new Date())
      .then((events) => (this.events = events));
  }
}
