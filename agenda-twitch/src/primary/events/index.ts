import EventsComponent from "@/primary/events/Events.component";
import EventsVue from "@/primary/events/Events.vue";

export { EventsComponent, EventsVue };
