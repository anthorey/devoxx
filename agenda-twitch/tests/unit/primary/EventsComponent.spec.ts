import { shallowMount, Wrapper } from "@vue/test-utils";
import { EventsComponent, EventsVue } from "@/primary/events";
import { CalendarEvents } from "@/domain/CalendarEvents";

let wrapper: Wrapper<EventsComponent>;
let component: EventsComponent;

const calendarEvents: CalendarEvents = {
  listNext: () =>
    Promise.resolve([
      {
        id: "abcdef",
        title: "First event",
        start: new Date("2021-09-16T06:00:00.000Z"),
        end: new Date("2021-09-16T07:00:00.000Z"),
      },
    ]),
};

const wrap = async (): Promise<void> => {
  wrapper = shallowMount<EventsComponent>(EventsVue, {
    provide: {
      calendarEvents: () => calendarEvents,
    },
  });
  component = wrapper.vm;
  await new Promise((resolve) => setTimeout(resolve, 0));
};

describe("Events Component", () => {
  it("Should exists", async () => {
    await wrap();
    expect(wrapper.exists()).toBe(true);
  });

  it("Should list events", async () => {
    await wrap();
    expect(component.events).toHaveLength(1);
  });
});
