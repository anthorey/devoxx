import fs from "fs";
import path from "path";

export const BASIC_ICS_CONTENT = fs
  .readFileSync(path.resolve(__dirname, "basic.ics"))
  .toString();
