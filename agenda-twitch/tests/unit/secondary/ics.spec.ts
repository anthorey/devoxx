import * as ical from "ical";
import { CalendarComponent, FullCalendar } from "ical";
import {
  CalendarElement,
  toCalendarEvent,
  toCalendarEvents,
} from "@/secondary/ics";
import { CalendarEvent } from "@/domain/CalendarEvent";
import { BASIC_ICS_CONTENT } from "./ics.fixture";

const calendar = (): FullCalendar => ical.parseICS(BASIC_ICS_CONTENT);

const firstEntry = (): CalendarElement => {
  const [entry] = Object.entries(calendar());
  return entry;
};

const firstCalendarComponent = (): CalendarComponent => {
  const [, calendarComponent] = firstEntry();
  return calendarComponent;
};

describe("ICS", () => {
  it("Should read an event", () => {
    const [key, event] = firstEntry();

    expect(key).toEqual("43du0vm989hms0cuotblrhebsm@google.com");
    expect(event.summary).toEqual("Code & Coffee TypeScript");
    expect(event.start).toEqual(new Date("2021-09-16T06:00:00.000Z"));
    expect(event.end).toEqual(new Date("2021-09-16T07:00:00.000Z"));
  });

  it("Should convert event to domain", () => {
    expect(toCalendarEvent(firstEntry())).toEqual<CalendarEvent>({
      id: "43du0vm989hms0cuotblrhebsm@google.com",
      title: "Code & Coffee TypeScript",
      start: new Date("2021-09-16T06:00:00.000Z"),
      end: new Date("2021-09-16T07:00:00.000Z"),
    });
  });

  it("Should list events", () => {
    const list = toCalendarEvents(calendar());
    const [first] = list;
    expect(list).toHaveLength(96);
    expect(first).toEqual<CalendarEvent>({
      id: "43du0vm989hms0cuotblrhebsm@google.com",
      title: "Code & Coffee TypeScript",
      start: new Date("2021-09-16T06:00:00.000Z"),
      end: new Date("2021-09-16T07:00:00.000Z"),
    });
  });
});
