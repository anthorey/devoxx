import { AxiosInstance, AxiosResponse } from "axios";
import sinon, { SinonStub } from "sinon";
import { BASIC_ICS_CONTENT } from "./ics.fixture";
import { CalendarEventsGoogle } from "@/secondary/CalendarEventsGoogle";
import { CalendarEvent } from "@/domain/CalendarEvent";

interface AxiosInstanceStub extends AxiosInstance {
  get: SinonStub;
}

const response = <T>(data: T): AxiosResponse<T> => ({ data } as AxiosResponse);

const stubAxiosInstance = (): AxiosInstanceStub =>
  ({
    get: sinon.stub(),
  } as AxiosInstanceStub);

describe("CalendarEventsGoogle", () => {
  it("Should get calendar events", async () => {
    const instance = stubAxiosInstance();
    instance.get.resolves(response(BASIC_ICS_CONTENT));

    const events = new CalendarEventsGoogle(instance);

    const calendarEvents = await events.listNext(
      new Date("2021-09-07T06:00:00Z")
    );

    const [uri] = instance.get.getCall(0).args;
    const [first] = calendarEvents;
    expect(calendarEvents).toHaveLength(3);
    expect(first).toEqual<CalendarEvent>({
      id: "7ed07p0d2bm5gilpne67q98eih@google.com",
      title: "Dans quel ordre faire les features ? (Colin)",
      start: new Date("2021-09-15T16:00:00.000Z"),
      end: new Date("2021-09-15T17:30:00.000Z"),
    });
    expect(uri).toBe("calendar.ics");
  });
});
