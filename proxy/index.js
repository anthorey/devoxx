const express = require('express');
const app = express();
const proxy = require('express-http-proxy')
const axios = require("axios");
const PORT = 4200;

app.get('/calendar.ics', (req, res) => axios.get('https://calendar.google.com/calendar/ical/c_07pbpmtv1a4d0iu48tnm6tl328%40group.calendar.google.com/public/basic.ics').then(response => res.send(response.data)).catch(console.error))
app.use('/', proxy('http://localhost:8080'));

app.listen(PORT, () => console.log(`Example app listening at http://localhost:${PORT}`))
